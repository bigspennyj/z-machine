#include <fstream>
#include <cstdint>
#include <iostream>
#include <iomanip>

#include "zmem.h"

//mem_block functions

uint16_t z_mem::read_word(int addr)
{
	uint16_t ret = 0;
	if (addr < 0 || addr >= len - 1) {
		throw std::runtime_error("out of bounds memory access!");
	}
	ret = mem[addr] << 8 | mem[addr + 1];
	return ret;
}

char z_mem::read_byte(int addr)
{
	if (addr < 0 || addr >= len) {
		throw std::runtime_error("out of bounds memory access!");
	}
	return mem[addr];
}

uint16_t z_mem::peek_word(const int& addr) const
{
	uint16_t ret = 0;
	if (addr < 0 || addr >= len - 1) {
		throw std::runtime_error("out of bounds memory access!");
	}
	ret = mem[addr] << 8 | mem[addr + 1];
	return ret;
}

char z_mem::peek_byte(const int& addr) const
{
	if (addr < 0 || addr >= len) {
		throw std::runtime_error("out of bounds memory access!");
	}
	return mem[addr];
}

z_mem::z_mem(std::ifstream& is) :
	mem(nullptr),
	static_mem(nullptr),
	dictionary(nullptr),
	global_vars(nullptr),
	objects(nullptr),
	high_mem(nullptr),
	len(-1)
{
	if (!is) {
		throw std::runtime_error("bad stream passed to zmem");
	}
	is.seekg(0, std::ios_base::end);
	len = is.tellg();
	is.seekg(0, std::ios_base::beg);

	mem = new char[len];
	if (is.read(mem, len)) {
		if (mem[0] != 3) {
			throw std::runtime_error("version not supported");
		}
		high_mem = mem + read_word(header_high_mem_addr);
		dictionary = mem + read_word(header_dictionary_addr);
		global_vars = mem + read_word(header_globals_addr);
		objects = mem + read_word(header_obj_table_addr);
		static_mem = mem + read_word(header_static_addr);
	}
}

