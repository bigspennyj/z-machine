#ifndef ZMEM_H
#define ZMEM_H

#include <fstream>
#include <cstdint>

class z_mem {
public:
	z_mem(std::ifstream& is);

	~z_mem()
	{
		delete[] mem;
	}
	
	char read_byte(int addr);
	uint16_t read_word(int addr);

	char peek_byte(const int& addr) const;
	uint16_t peek_word(const int& addr) const;

	static int constexpr header_length = 64;
	static int constexpr header_high_mem_addr = 0x4;
	static int constexpr header_pc_addr = 0x6;
	static int constexpr header_dictionary_addr = 0x8;
	static int constexpr header_obj_table_addr = 0xa;
	static int constexpr header_globals_addr = 0xc;
	static int constexpr header_static_addr = 0xe;

private:
	char *mem;
	char *static_mem;
	char *dictionary;
	char *global_vars;
	char *objects;
	char *high_mem;
	int len;
};

#endif
