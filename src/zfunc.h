#ifndef ZFUNC_H
#define ZFUNC_H

#include <cstdint>
#include <string>

struct z_func_args {
	int num_operands;
	unsigned char optypes[8];
	uint16_t operands[8];
	unsigned char store_loc;
	std::string text;
};

typedef void (*z_func_p)(const z_func_args& args);

struct z_func {
	unsigned char opcode;
	z_func_args args;
	z_func_p fn;
	void operator()()
	{
		if (fn)
			fn(args);
	}
};

/*
namespace z_func_impl {
	void rtrue(const z_func_args& args);
	void je(const z_func_args& args);
} //namespace z_func_impl
*/

#endif
