#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <cerrno>
#include <iomanip>
#include "zmachine.h"

int main(void)
{
	try {
		z_machine machine("data/ZORK1.DAT");
		machine.fetch_next_instruction();
	} catch (std::runtime_error e) {
		std::cout << e.what() << std::endl;
	}
}
