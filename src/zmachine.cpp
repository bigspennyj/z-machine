#include <iostream>
#include <fstream>
#include <cstdint>
#include <cstring>

#include "zmachine.h"
#include "zfunc.h"

z_machine::z_machine(const std::string& datafile) : memory(nullptr), ip(0)
{
	std::ifstream is(datafile);
	if (is) {
		memory = new z_mem(is);
	}
	ip = memory->read_word(z_mem::header_pc_addr);
	std::cout << "Address of initial IP: " << std::hex << ip << std::endl;
}

void z_machine::run()
{
	//z_func current_instruction = fetch_next_instruction();
	//current_instruction();
}

void z_machine::fetch_next_instruction()
{
	unsigned char opcode = memory->peek_byte(ip);
	unsigned char optype = (opcode >> 6) & 0x3;
	z_func ret;

	switch (optype) {
		case 0x3: {
			std::cout << "VARIABLE:" << std::endl;
			decode_var_instruction(ret);
			break;
		}
		case 0x2: {
			std::cout << "SHORT:" << std::endl;
			decode_short_instruction(ret);
			break;
		}
		default: {
			std::cout << "LONG:" << std::endl;
			decode_long_instruction(ret);
			break;
		}
	}

	std::cout << std::hex << "opcode number: " << +ret.opcode << std::endl;
	std::cout << "has " << ret.args.num_operands << " operands" << std::endl;
	std::cout << "store location: " << +ret.args.store_loc << std::endl;
	for (int i = 0; i < ret.args.num_operands; i++) {
		std::cout << "operand " << i << ": " << +ret.args.operands[i] << std::endl;
	}
}

void z_machine::decode_short_instruction(z_func& ret)
{
	unsigned char raw, opcode = 0, operand = 0;
	raw = ip_fetch_byte();
	opcode = raw & 0x0f;
	operand = raw & 0x30;
	z_func_args args;

	if (operand == 0x30) { //0op
		args.num_operands = 0;
	} else {
		if (operand == 0x00) {
			uint16_t op_val = ip_fetch_word();
			args.num_operands = 1;
			args.optypes[0] = operand;
			args.operands[0] = op_val;
		} else {
			char op_b = ip_fetch_byte();
			args.num_operands = 1;
			args.optypes[0] = operand;
			args.operands[0] = op_b;
		}
		if ((opcode >= 1 && opcode <= 4) || opcode == 8 || opcode == 0xe || opcode == 0xf) {
			args.store_loc = ip_fetch_byte();
		}
	}
	ret.fn = nullptr;
	ret.args = args;
}

void z_machine::decode_long_instruction(z_func& ret)
{
	char raw;
	char op1 = 0, op2 = 0, opcode = 0;
	unsigned char op1_type, op2_type;

	raw = ip_fetch_byte();
	opcode = raw & 0x1f;

	op1 = ip_fetch_byte();
	op2 = ip_fetch_byte();
	
	op1_type = (raw & 0x40);
	op2_type = (raw & 0x20);

	z_func_args args;
	args.num_operands = 2;
	args.optypes[0] = op1_type;
	args.optypes[1] = op2_type;
	args.operands[0] = op1;
	args.operands[1] = op2;

	if (opcode == 8 || opcode == 9 || (0x0f <= opcode && opcode <= 0x19)) {
		args.store_loc = ip_fetch_byte();
	}

	ret.fn = nullptr;
	ret.opcode = opcode;
	ret.args = args;
}

void z_machine::decode_var_instruction(z_func& ret)
{
	unsigned char raw, vartypes, opcode;
	raw = ip_fetch_byte();
	vartypes = ip_fetch_byte();
	z_func_args args;
	int i = 0;
	
	opcode = raw & 0x1f;
	while (vartypes > 0 && i < 7) {
		unsigned char argtype = vartypes & 0xc0;
		if (argtype == 0xc0)
			break;
		args.optypes[i++] = argtype;
		vartypes = vartypes << 2;
	}
	args.num_operands = i;

	for (i = 0; i < args.num_operands; i++) {
		if (args.optypes[i] == 0) {
			args.operands[i] = ip_fetch_word();
		} else {
			args.operands[i] = ip_fetch_byte();
		}
	}

	if (opcode == 0 || opcode == 7) {
		args.store_loc = ip_fetch_byte();
	}
	ret.fn = nullptr;
	ret.opcode = opcode;
	ret.args = args;
}

unsigned char z_machine::ip_fetch_byte()
{
	unsigned char ret = memory->read_byte(ip);
	ip++;
	return ret;
}

uint16_t z_machine::ip_fetch_word()
{
	uint16_t ret = memory->read_word(ip);
	ip += 2;
	return ret;
}
