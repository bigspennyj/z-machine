#ifndef ZMACHINE_H
#define ZMACHINE_H

#include <iostream>
#include <cstdint>

#include "zmem.h"
#include "zfunc.h"

class z_machine {
public:

	z_machine(const std::string& datafile);

	~z_machine()
	{
		delete memory;
	}

	void fetch_next_instruction();
	void run();
	void decode_long_instruction(z_func&);
	void decode_short_instruction(z_func&);
	void decode_var_instruction(z_func&);

private:
	uint16_t ip_fetch_word();
	unsigned char ip_fetch_byte();
	z_mem *memory;
	int ip;
};

#endif
