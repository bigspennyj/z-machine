# z-machine

This is my implementation of Infocom's Z-Machine.

The Z-Machine is a virtual machine designed to play Infocom's text adventures.
It was built to try and overcome memory limitations of the day.  If you'd like
more information about it, the digital antiquarian has a great post about it
[here.](http://www.filfre.net/2012/01/zil-and-the-z-machine/)  It's worth a read!

I'm not accepting any contributions at this time as I'm writing this
as a learning exercise (which, as you can tell, I've only just started).
Feedback is welcome, however!  If you'd like to try writing a z-machine yourself, 
check out the 
[z-machine standards document](http://inform-fiction.org/zmachine/standards/z1point1/index.html)
for details about how it works.

Contact me at `spencerdleslie at gmail` if you have feedback or questions.

Thanks!
