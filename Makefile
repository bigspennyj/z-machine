CC=g++
SRCDIR=src
BINDIR=bin
OBJDIR=bin/obj
CCFLAGS=-W -Wall -pedantic -std=c++11

_DEPS=zfunc.h zmachine.h zmem.h
DEPS=$(patsubst %,$(SRCDIR)/%,$(_DEPS))

_OBJ=main.o zmachine.o zmem.o
OBJ=$(patsubst %,$(OBJDIR)/%,$(_OBJ))

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp $(DEPS)
	$(CC) -c $(CFLAGS) -o $@ $<

zork : $(OBJ)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^
